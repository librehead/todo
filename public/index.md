---
title: TODO
---

## Projects to work on

- Go channels in C++, combined with a thread pool to kind of "simulate" preemptive scheduling.
- Render vector graphics (CPU/GPU). Look at forma. Use a LISP as the description language (to be at least as powerful as SVG). Include animations (with stuff like morphing), filter effets, etc. Build a utility that converts this to SVG/PNG/GIF/Lottie/Video/etc. Include more primitives, like arrows (take a look at TikZ maybe?). Add an effect for the wave function collapse, and linear/bilinear/bicubic interpolation for images.
- Ray tracer.
- Read "Software Foundations".
- Implement a programming language with: Go/Eralng-style concurrency, refinement types, algebraic effects, GADTs, OCaml module system, typeclasses, a default JS backend that makes use of a hybrid between MVC/FRP (with compiler-inferred DOM diffing). Make a shader abstraction with it, making use of the type system. The type system could also be leveraged to make a WebSockets abstraction, shared between client and server (?).
- Shell with SmallTalk as the language, written in Haskell.
- Make a synth using CGAL (Rust) and FM. Again, use LISP as the description language for notes/instruments.

#### Compiling

- Simple bytecode for expressions in SML (Haskell-Math-Parser but adapted to SML)
- Regex compiler in Rust
- Implement a memory allocator (either buddy or slab)
- Implement a file system
- Implement a Prolog WAM
- Implement a Forth interpreter/compiler
- Implement an APL interpreter
- Implement a LISP
- Implement a programming language:
  * Implement an assembly emulator (your own ISA, make the syntax as a LISP)
  * Implement your own IR (again a LISP, maybe similar to LLVM IR, SSA-form)
  * A lanugage that has algebraic effects (using monomorphization) and possibly a zero-pause GC (reference counting using a global "holder" so that the cycle problem is solved (it's probably not an optimal GC but who cares). Could be an ML-derivative with go-style concurrency
- Image classifier in pure lisp
- [Higher-Order Logic for prototyping functional programming languages with sophisticated type systes](http://adam.chlipala.net/papers/MakamICFP18/)
- [Implement dependent type theory](http://math.andrej.com/2012/11/08/how-to-implement-dependent-type-theory-i/)
- Look at [plzoo](https://github.com/andrejbauer/plzoo). Lots of programming paradigms implemented, source-code is annotated with comments

#### Algorithms/Data Structures

- Build an async runtime in Rust following the sender/receiver model from C++'s proposals, try to use function pointer as much as possible instead of closures, make a (very simple) redis clone as a test
- Implement a database
- Implement stuff from the algorithms-related books I've saved (and the list of algorithms/data structures from Wikipedia)
- Implement a neural network from scratch in SML, take a look [here](http://www-cs-students.stanford.edu/~blynn/haskell/brain.html) and [here](http://h2.jaguarpaw.co.uk/posts/refactoring-neural-network/)
- [Writing A Document Database From Scratch In Go](https://notes.eatonphil.com/documentdb.html)
- BigNum library
- [Implement an autodiff engine](https://jingnanshi.com/blog/autodiff.html)
- Redo my courses in cryptography and implement them interactively

#### Graphics

- Render distance-estimated fractals (rust-wgpu)
- Implement a vector graphics lib (CPU) with an API similar to OpenVG
- [Implement Mesh Gradients in Haskell](https://twinside.github.io/coon_rendering.html)
- Implement an SDF-like renderer for text but store the curve directly instead of the distance to it and compute the distance in the shader (see: "A primer on Bezier curves" for how to compute the distance), use rust-wgpu
- Implement the "Easy scalable text rendering on the GPU" method and maybe try to adopt it for general vector graphics, user rust-wgpu
- Follow "Ray Tracing from the ground up" in rust-wgpu
  * Maybe look at this course as well (seems lighter): [Ray Tracing From Scratch](https://github.com/nikolausmayer/raytracing-from-scratch/). [Here](https://www.ffconsultancy.com/languages/ray_tracer/comparison_cpp_vs_sml.html) is a very simple implementation of an acceleration structure. Maybe take this opportunity to implement a ray tracer in various programming languages with different paradigms: Rust, SML/Haskell, Forth, APL/J, Prolog, Lisp, Koka. [This](https://jacco.ompf2.com/2022/04/13/how-to-build-a-bvh-part-1-basics/) might be an excellent resource to look into for building BVHs.
- Etmull-Clark subdivision surface algorithm implementation
- Compute software rasterizer with WebGPU, [maybe look at this](https://github.com/OmarShehata/webgpu-compute-rasterizer/blob/main/how-to-build-a-compute-rasterizer.md)
- Implement marching cubes/squares
- Implement ray tracing directly from Bézier patching as described [here](https://www.mattkeeter.com/projects/mrep/)

#### DSP

- Build a synth in Rust and make a visualizer for songs (user wgpu). Implement a description language for sound (look at CSound) (read the DSP books I've saved). Maybe take inspiration from [this](https://blog.demofox.org/diy-synthesizer/)?
- Build an image/audio fingerprint (image/audio search) similar to google image search/shazam
- Implement a PNG/OGG encoder/decoder in Rust
- Build a visualizer for the Fourier Transform in rust-wgpu
- Noise reduction on images/audio
- Build an image editor (can be a CLI, the algorithms are important)

#### Low-level stuff

- Gameboy emulator in OCaml
- CPU emulator in HardCaml (or maybe that retro book in clash)
- Pong FPGA
- Game of life FPGA
- Learn Verilog/VHDL
- Build a simple OS (follow selfie or that 0x00sec blog)
- Nandgame, Nand2Tetris

#### Various

- Build a compiler in SML or Koka (not decided yet) that compiles a description language to HTML/CSS/JS for nice visualiztions
  * Implement a visualizer for atomic operations
- Build a OSS License database in Prolog, basically an interactive/programmatic way to see what one can do with a license (or more licenses in the same project - compatibilities etc.)
- Use TLA+ to formally verify a ring buffer
- Build a search engine
- Bulid a JIT (maybe for Lua?)
- Implement a RNG
- Implement crypto algorithms in Rust (maybe a blockchain as well)
- Build a git

## Cărți/Articole de citit

Pentru fiecare carte citită fă un LaTeX/pandoc (mai degrabă un pandoc care să genereze HTML pe care să-l pun pe github pages ca să-l am la îndemână) în care pui ce e relevant (definiții/teoreme/algoritmi/etc. + capitolul și pagina la care se găsesc). Pentru cărțile de mate în special se pot face vizualizări ca pagini HTML cu Three.js. La cărțile la care se poate, am scris ce pot face ca să combin teoria cu practica.

PLT, logică, type theory:

- [SICP](https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/book.html) - implement the exercises in Scheme in parallel
- [Introduction To Algorithms](https://www.amazon.com/Introduction-Algorithms-3rd-MIT-Press/dp/0262033844) - implement them in parallel
- [Classical Logic](https://plato.stanford.edu/entries/logic-classical/)
- [Forall X](https://www.fecundity.com/logic/)
- [Reasoning About Knowledge](https://mitpress.mit.edu/books/reasoning-about-knowledge)
- [The Hitchhiker's Guide To Logical Verification](https://cs.brown.edu/courses/cs1951x/static_files/main.pdf) - learn Lean
- [Types And Programming Languages](https://www.cis.upenn.edu/~bcpierce/tapl/)
- [Thinking With Types](https://thinkingwithtypes.com/) - Type-Level Programming In Haskell
- [The Little Typer](https://mitpress.mit.edu/books/little-typer)
- [Practical Foundations For Programming Languages](https://www.cs.cmu.edu/~rwh/pfpl.html) - Robert Harper
- [Practical Theory Of Programming](https://www.cs.toronto.edu/~hehner/aPToP/aPToP.pdf)
- [Type Theory](https://plato.stanford.edu/entries/type-theory/)
- [Software Foundations](https://softwarefoundations.cis.upenn.edu/) - compile every chapter with coq and track the progress
- [Category Theory For Programmers](https://bartoszmilewski.com/2014/10/28/category-theory-for-programmers-the-preface/) or [Basic Category Theory For Computer Scientists](https://mitpress.mit.edu/books/basic-category-theory-computer-scientists)
- [Introduction To Univalent Foundations Of Mathematics With Agda](https://www.cs.bham.ac.uk/~mhe/HoTT-UF-in-Agda-Lecture-Notes/) - solve the exercises in parallel

Fun to try (ultimately the goal would be writing an emulator or a compiler):

- [Neural Network In Pure Lisp](https://woodrush.github.io/blog/posts/2022-01-16-neural-networks-in-pure-lisp.html) - using SectorLisp, no built-in stuff, really great way to learn
- [Buddy Memory Allocator](https://github.com/red-rocket-computing/buddy-alloc/blob/master/doc/bitsquid-buddy-allocator-design.md) - or take a look at slab allocators. Combine this with Andrei Alexandrescu's `std::allocator` talk (composable design).
- [8-Puzzle problem in Prolog](https://www.cpp.edu/~jrfisher/www/prolog_tutorial/5_2.html)
- [VM in prolog](https://www.metalevel.at/tist/)
- [Writing a minimal Lua VM in Rust](https://notes.eatonphil.com/lua-in-rust.html)
- [How to Write a JIT Compiler](https://github.com/spencertipping/jit-tutorial)
- [Haskell-Math-Parser](https://github.com/tadeuzagallo/haskell-math-parser/tree/master/complete)
- A language that compiles to SQL
- A search engine similar to milisearch, maybe look at [Let's Build A Full-Text Search Engine](https://artem.krylysov.com/blog/2020/07/28/lets-build-a-full-text-search-engine/), or [Building A Full-Text Search Engine In 150 Lines Of Python Code](https://bart.degoe.de/building-a-full-text-search-engine-150-lines-of-code/). Use C++ with the sender/receiver model and `io_uring` for the implementation of the search engine server
- [Write A File System From Scratch In Rust](https://blog.carlosgaldino.com/writing-a-file-system-from-scratch-in-rust.html). [This](https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/project06.html) might also be useful
- [Implement a RNG](https://www.pcg-random.org/index.html) - follow this paper and develop your own implementation.
- Implement compilers/interpreters for:
  * A statically-typed language with GC that's syntactically similar to Rust
  * A LISP
  * A prolog interpreter (WAM)
  * A Forth compiler/interpreter
  * An APL interpreter

Parsing (theory and applications):

- [Automata, Computability, Complexity Theory And Applications](https://www.cs.utexas.edu/~ear/cs341/automatabook/) - Elaine Rich (or [Elements of The Theory Of Computation](https://www.pearson.com/store/p/elements-of-the-theory-of-computation/P100001374025/9780132624787)), make a regex matcher in parallel for the first part, then make a simple parser for the second part, then make a turing machine visualizer, implement a Turing Machine with C++ templates and Haskell.
- [Proofs, Computability, Undecidability, Complexity, And the Lambda Calculus: An Introduction](https://www.cis.upenn.edu/~cis511/notes/proofslambda.pdf)
- [Parsing Techniques: A Practical Guide](https://dickgrune.com/Books/PTAPG_2nd_Edition/) - Dick Grune, Ceriel J.H. Jacobs, make a parser in parallel
- [Engineering A Compiler](https://www.amazon.com/Engineering-Compiler-Keith-Cooper/dp/012088478X) - Keith D. Cooper & Linda Torczon, make a full compiler, with codegen, in parallel (maybe starting from the language(s) made above)
- [Modern Compiler Implementation In ML](https://www.cs.princeton.edu/~appel/modern/ml/) - excellent read, even teaches advanced concepts like code generation and GC.

Concurrency and parallel programming, computer archtecture:

- [Is Parallel Programming Hard, And, If So, What Can You Do About It?](https://mirrors.edge.kernel.org/pub/linux/kernel/people/paulmck/perfbook/perfbook.html) - implement parallel (and lock-free) algorithms and data structures in parallel
- [A Primer On Memory Consistency And Cache Coherence](https://www.morganclaypool.com/doi/abs/10.2200/S00962ED2V01Y201910CAC049) - maybe implement some of the algorithms in parallel

Crypto:

- [The Joy Of Cryptography](https://joyofcryptography.com/) - save the definitions, maybe also implement some of the algorithms
- [A Graduate Course In Applied Cryptography](https://news.ycombinator.com/item?id=28918554) - same as above

Networking and distributed systems:

- [Computer Networking: Principles, Protocols And Practice](https://www.computer-networking.info/2nd/html/) - maybe fiddle with docker and mess with dns and watch Ben Eater in parallel
- [Notes On Theory Of Distributed Systems](http://cs-www.cs.yale.edu/homes/aspnes/classes/465/notes.pdf) - implement some of the algorithms/data structures in parallel

Graphics:

- [Intro To Game Programming](https://www.youtube.com/playlist?list=PL_xRyXins848jkwC9Coy7B4N5XTOnQZzz)
- [LearnOpenGL](https://learnopengl.com/) - implmenet this in wgpu
- [Global Illumination Compendium](https://people.cs.kuleuven.be/~philip.dutre/GI/)
- [Ray Tracing From The Ground-Up](https://www.amazon.com/Ray-Tracing-Ground-Kevin-Suffern/dp/1568812728) - implement this stuff in Rust with WebGPU directly
- [Physically-Based Rendering: From Theory To Implementation](https://www.pbr-book.org/)
- [Computational Geometry - Algorithms And Applications](https://link.springer.com/book/10.1007/978-3-540-77974-2) - lots of visualizations can be made with this, it's geometry after all..
- [Geometry And Algorithms For Computer-Aided Design](https://www2.mathematik.tu-darmstadt.de/~ehartmann/cdgen0104.pdf) - implement a simple CAD app in parallel
- [Basic 2D Rasterization](https://magcius.github.io/xplain/article/rast1.html)
- [2D Vector Graphics Course From Diego Nehab](https://w3.impa.br/~diego/teaching/2021.0/index.html) - implement a renderer in parallel
- [Distance-Estimated 3D Fractals](http://blog.hvidtfeldts.net/index.php/2011/06/distance-estimated-3d-fractals-part-i/) - it shouldn't be too hard to implement something like this.
- [WebGPGPU](https://github.com/michaelerule/webgpgpu) - image processing stuff

A bit off-topic, but still an interesting read:

- [Color Vision](https://www.handprint.com/LS/CVS/color.html) - note down the important bits.

Information theory:
- [Elements of Information Theory](https://www.wiley.com/en-us/Elements+of+Information+Theory,+2nd+Edition-p-9780471241959) - implement any data structures/algorithms.
- [Introduction To Information Retrieval](https://nlp.stanford.edu/IR-book/information-retrieval-book.html) - maybe make a search engine in parallel.

AI:

- [MIT Artificial Intelligence Course](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-034-artificial-intelligence-fall-2010/) - implement A\*, Minimax for a fifteen-puzzle/chess-engine problem. Make a neural net visualizer.

Audio:

- [The Scientist And Engineer's Guide To DSP](https://www.analog.com/en/education/education-library/scientist_engineers_guide.html) - make a synth, JPEG/PNG encoder/decoder and a speech synthetizer maybe? Other ideas: image search, audio fingerprinting, FFT visualization. I could try to make visualizations directly on the web: [Building a Wavetable Synthesizer From Scratch with Rust, WebAssembly, and WebAudio](https://cprimozic.net/blog/buliding-a-wavetable-synthesizer-with-rust-wasm-and-webaudio/). I've also got more related links saved. This book may also help, maybe it's more accessible/practical: [Understanding DSP](https://www.amazon.com/Understanding-Digital-Signal-Processing-3rd/dp/0137027419). This one is focused on music regarding DSP: [The Computer Music Tutorial](https://mitpress.mit.edu/books/computer-music-tutorial)
- [FunDSP](https://github.com/SamiPerttu/fundsp) - I could borrow some stuff from here (for implementing a synth).
- [Signals And Systems](https://www.amazon.com/Signals-Systems-2nd-Alan-Oppenheim/dp/0138147574) - Alan Oppenheim, Alan Willsky
- [Julius Orion Smith III](https://ccrma.stanford.edu/~jos/) - lots of excellent books on DSP, including the one which is "Mathemathics of the Discrete Fourier Transform". There are others as well, including one on filters. I could implement some visualizations in parallel.

Algorithms:

- [Algorithmica](https://en.algorithmica.org/)
- [Implementing The Viola-Jones Face-Detection Algorithm](https://web.archive.org/web/20181021023153/http://etd.dtu.dk/thesis/223656/ep08_93.pdf)
- [Real-World Dynamic Programming: Seam Carving](https://avikdas.com/2019/05/14/real-world-dynamic-programming-seam-carving.html) - also look at the [improved](https://avikdas.com/2019/07/29/improved-seam-carving-with-forward-energy.html) version
- [Algorithms, 4th Edition](https://algs4.cs.princeton.edu/home/) - more practical and easier to digest than "Introduction to Algorithms".
- [Damn Cool Algorithms: Levenshtein Automata](http://blog.notdot.net/2010/07/Damn-Cool-Algorithms-Levenshtein-Automata) - levenshtein but for a search engine, implement a crude search engine in parallel.

Databases:

- [Theory Of Relational Databases](https://web.cecs.pdx.edu/~maier/TheoryBook/TRD.html)
- Implementing a database: [How Does A Database Work?](https://cstack.github.io/db_tutorial/), [Writing An SQL Database From Scratch In Go](https://notes.eatonphil.com/database-basics.html), [Write A Time-Series Database Engine From Scratch](https://nakabonne.dev/posts/write-tsdb-from-scratch/), [Implementing A Key-Value Store](https://codecapsule.com/2012/11/07/ikvs-implementing-a-key-value-store-table-of-contents/), [Implementation Of A B-Tree Database Class](https://www.codeproject.com/Articles/7410/Implementation-of-a-B-Tree-Database-Class)

Engineering:

- [Mathematics For Computer Science](https://courses.csail.mit.edu/6.042/spring17/mcs.pdf) - Eric Lehman
- [Thirty-three Miniatures: Mathematical and Algorithmic Applications of Linear Algebra](https://kam.mff.cuni.cz/~matousek/stml-53-matousek-1.pdf)
- [Linear Algebra Done Right](https://linear.axler.net/) - Sheldon Axler, maybe make some visualizations yourself
- [Interactive Linear Algebra](https://textbooks.math.gatech.edu/ila/) - same as above
- [Probability And Statistics For Engineers & Scientists](https://math.buet.ac.bd/public/faculty_profile/files/835598806.pdf). Additionally: [Stat Trek](https://stattrek.com/) and [Probability Course](https://www.probabilitycourse.com/). Maybe make some visualizations (esp. for distributions)
- [Calculus Made Easy](http://calculusmadeeasy.org/)
- [Div, Grad, Curl Are Dead](https://people.ucsc.edu/~rmont/papers/Burke_DivGradCurl.pdf)
- [They Feynmann Lectures On Physics](https://www.feynmanlectures.caltech.edu/)
- [Mathematical Methods For Physics And Engineering](https://www.amazon.com/dp/0521679710) - K. F. Riley, M. P. Hobson, S. J. Bence
- [Numerical Methods For Scientists And Engineers](https://www.amazon.com/Numerical-Methods-Scientists-Engineers-Mathematics/dp/0486652416)
- [Structure And Interpretation Of Classical Mechanics](https://groups.csail.mit.edu/mac/users/gjs/6946/sicm-html/book.html)
- [Structures: Or Why Things Don't Fall Down](https://www.amazon.com/Structures-Things-Dont-Fall-Down/dp/0306812835)
- [Basic Civil Engineering](https://www.amazon.com/Comprehensive-Basic-Civil-Engineering-Punmia/dp/8170084032)
- [Surveying And Levelling](https://www.amazon.com/Surveying-Levelling-2Ed-Basak/dp/9332901538)
- [Shigley's Mechanical Engineering Design](https://www.amazon.com/Shigleys-Mechanical-Engineering-Design-McGraw-Hill/dp/0073398209) - 10th ed., Richard G. Budynas, J. Keith Nisbett
- [Fundamentals Of Thermodynamics](https://www.wiley.com/en-us/Fundamentals+of+Thermodynamics%2C+10th+Edition-p-9781119494966) - Claus Borgnakke, Richard E. Sonntag
- [Electrochemical Engineering](https://www.wiley.com/en-us/Electrochemical+Engineering-p-9781119004257) - Thomas F. Fuller, John N. Harb
- [Economics Book](https://www.amazon.com/Economics-Paul-Samuelson/dp/0073511293) - Paul Samuelson, it doesn't hurt to learn more about economics...

Lower-level stuff:

- [Practice Problems For Hardware Engineers](https://arxiv.org/abs/2110.06526)
- [Nandgame](https://nandgame.com/)
- [Verilog, Formal Verification And Verilator Beginner's Tutorial](https://zipcpu.com/tutorial/). Also look at it's referenced tutorials, there's some great stuff there
- [Conway's Game Of Life On FPGA](https://k155la3.blog/2020/10/09/conways-game-of-life-on-fpga/)
- [FPGA Tutorial](https://fpgatutorial.com/)
- [Learning Hardware Programming As A Software Engineer](https://blog.athrunen.dev/learning-hardware-programming-as-a-software-engineer/)
- [Pong At The Circuit Level](https://www.falstad.com/pong/index.html)
- [Reconstructing Pong On An FPGA](http://www.cs.columbia.edu/~sedwards/papers/edwards2012reconstructing.pdf)
- [Retrocomputing With Clash](https://unsafeperform.io/retroclash/) - Haskell for FPGA hardware design
- [HardCaml MIPS](https://ceramichacker.com/blog/1-1x-hardcaml-mips-intro-what-and-why) - building a MIPS CPU with HardCaml
- [Selfie](https://github.com/cksystemsteaching/selfie) - Educational platform for teaching undergraduate and graduate students the design and implementation of programming languages and runtime systems - solve the exercises and run the autograder. It may also help to follow [Realmode Assembly - Writing bootable stuff](https://0x00sec.org/t/realmode-assembly-writing-bootable-stuff-part-1/2901) for a lighter intro into OS-Dev.
- [Nand2Tetris](https://www.nand2tetris.org/)
- [Computer Architecture: A Quantitative Approach](https://www.amazon.com/Computer-Architecture-Quantitative-John-Hennessy/dp/012383872X) - John L. Hennessy, David A. Patterson
- [Writing A Game Boy Emulator In OCaml](https://linoscope.github.io/writing-a-game-boy-emulator-in-ocaml/) - follow this by yourself
